//------------ NODE REQUIRES ---------------------------------------------------

const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const config = require('./server.infrastructure/config/config');
const jwt = require('jsonwebtoken');
const cors = require('cors');
const morgan = require('morgan');
const handlebars = require('handlebars');
const archiver = require('archiver');
const helpers = require('handlebars-helpers')({
  handlebars: handlebars
});
const fs = require('fs');
const bcrypt = require('bcryptjs');
const appArguments = require('optimist').argv;
global.lastLoggedUser = null;

//------------- NODE CONFIG -------------------------------------------------
app.use(cors());
app.set('trust proxy', true);

app.use(function (req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "http://localhost:8080");
  res.setHeader('Access-Control-Allow-Methods', 'POST, PUT, GET, DELETE, OPTIONS');
  res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


app.set('secret', config.secret);
app.use(morgan('dev'));
const router = express.Router();
router.use(bodyParser.urlencoded({extended: true, limit: '50mb'}));
router.use(bodyParser.json({limit: '50mb'}));
process.env.TMPDIR = './APP_DATA/tmp';

//------------- SERVER OPTIONS -------------------------------------------------

if (appArguments.mode) {
  switch (appArguments.mode) {
    case "public":
      app.use(express.static(__dirname + config.dirPaths.public));
      app.use(config.dirPaths.app_data.main, express.static(__dirname + config.dirPaths.app_data.main));
      server.listen(8081);
      console.log('#SERVER: Running on port 8081 and PUBLIC mode...');
      break;
    case "dev":
      app.use(express.static(__dirname + config.dirPaths.dev));
      app.use(config.dirPaths.app_data.main, express.static(__dirname + config.dirPaths.app_data.main));
      app.use('/bower_components', express.static(__dirname + '/bower_components'));
      server.listen(8080);
      console.log('#SERVER: Running on port 8080 and DEVELOPMENT mode...');
      break;
    default:
      app.use(express.static(__dirname + config.dirPaths.dev));
      app.use(config.dirPaths.app_data.main, express.static(__dirname + config.dirPaths.app_data.main));
      app.use('/bower_components', express.static(__dirname + '/bower_components'));
      server.listen(8080);
      console.log('#SERVER: Running on port 8080 and DEVELOPMENT mode...');
      break;
  }
}
else {
  app.use(express.static(__dirname + config.dirPaths.dev));
  app.use(config.dirPaths.app_data.main, express.static(__dirname + config.dirPaths.app_data.main));
  app.use('/bower_components', express.static(__dirname + '/bower_components'));
  server.listen(8081);
  console.log('#SERVER: Running on port 8080 and DEVELOPMENT mode...');
}

mongoose.Promise = global.Promise;

//------------ DB CONNECTION ---------------------------------------------------

const localDbUrl = config.dbUrls.local;
//connection by tunel in putty
const remoteDbUrl = config.dbUrls.remote;
//connection with local db on jack.metal.agh.edu.pl
const jackDbUrl = config.dbUrls.jackServer;

let dbUrl = '';

if (appArguments.db) {
  switch (appArguments.db) {
    case "remote":
      dbUrl = remoteDbUrl;
      console.log('#DATABASE: Connecting to REMOTE... ');
      break;
    case "local":
      dbUrl = localDbUrl;
      console.log('#DATABASE: Connecting to LOCAL... ');
      break;
    case "jack":
      dbUrl = jackDbUrl;
      console.log('#DATABASE: Connecting to jack.metal.agh.edu.pl... ');
      break;
    default:
      dbUrl = remoteDbUrl;
      console.log('#DATABASE: Connecting to DEFAULT (REMOTE)... ');
      break;
  }
}
else {
  dbUrl = remoteDbUrl;
  console.log('#DATABASE: Connecting to DEFAULT (REMOTE)... ');
}

mongoose.connect(dbUrl, {
    useMongoClient: true
  },
  function (err, db) {
    if (err) {
      console.log('#DATABASE: Unable to connect to server. \nError:', err.message);
    }
    else {
      console.log('#DATABASE: Connection established to', dbUrl);
    }
  }
)
;

function exitHandler(options, err) {
  if (options.cleanup)
    console.log('clean');
  if (err)
    console.log(err.stack);
  if (!options.exit)
    console.log('Server unknown error!');
  if (options.exit) {
    console.log('exit');
    process.exit(0);
  }
}

io.sockets.on('connection', function (socket) {
  socket.emit('connectedToSocket', {socket: socket.id});
  socket.on('change_room', function (data) {
    socket.join(data.userId);
    console.log('room changed to ' + data.userId);
  });
});

process.on('exit', exitHandler.bind(null, {exit: true}));

process.on('SIGTERM', exitHandler.bind(null, {exit: true}));

process.on('uncaughtException', exitHandler.bind(null, {exit: false}));

//------------ LOAD ROUTES ---------------------------------------------------

//Token not needed - must be included before authenticate
require('./server.infrastructure/routes/appInfo')(router);
require('./server.infrastructure/routes/authenticate')(router, app, jwt, bcrypt, io);
//Require token
require('./server.infrastructure/routes/appConfig')(router, bcrypt);
require('./server.infrastructure/routes/material')(router, fs, config);
require('./server.infrastructure/routes/process')(router, handlebars, fs, config, io, archiver);
require('./server.infrastructure/routes/chemicalElement')(router);
app.use('/api', router);
