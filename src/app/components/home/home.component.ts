import {Component, OnInit, ViewChild} from '@angular/core';
import {ConfigService} from "../../services/config.service";
import {MaterialsService} from "../../services/materials.service";
import {AuthenticationService} from "../../services/authentication.service";
import {ProcessesService} from "../../services/processes.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public stats: any = {};
  public appInfo: any = {};

  constructor(private configService: ConfigService, private materialService: MaterialsService, private processesService: ProcessesService, private authService: AuthenticationService) {

  }

  private getAppInfo() {
    this.configService.getAppConfig().then((response) => {
      this.appInfo = response;
    });
  }

  private getMaterialsCount() {
    this.materialService.getMaterialsCount().then((response) => {
      this.stats.materialsCount = response;
    });
  }

  private getProcessesCount() {
    this.processesService.getProcessesCount().then((response) => {
      this.stats.processesCount = response;

    });
  }

  ngOnInit() {
    this.getAppInfo();
    this.getMaterialsCount();
    this.getProcessesCount();
  }

}
