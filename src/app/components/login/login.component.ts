import {Component, OnInit} from '@angular/core';
import {ToastrService} from "ngx-toastr";
import {AuthenticationService} from "../../services/authentication.service";
import {DataService} from "../../services/data.service";
import {Router} from "@angular/router";
import {ConfigService} from "../../services/config.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public loading = false;
  public adminInfo = null;

  constructor(private configService: ConfigService, private toastrService: ToastrService, private authenticationService: AuthenticationService, private dataService: DataService, private router: Router) {
  }

  public getAppInfo = function () {
    this.configService.getAppConfig().then((response) => {
      this.adminInfo = response.admin;
    });
  };

  public login = function (loginFormValues: any) {
    this.loading = true;
    this.authenticationService.login(loginFormValues.username, loginFormValues.password).then((response) => {
      if (response.success === true) {
        this.dataService.loggedUser = response.user;
        this.router.navigate(['home']);

      }
    }, (error) => {
      this.clear();
    });
  };

  public clear = function (loginForm: any) {
    loginForm.resetForm();
    this.loading = false;
  };

  ngOnInit() {
    this.authenticationService.logout();
    this.getAppInfo();
  }
}
