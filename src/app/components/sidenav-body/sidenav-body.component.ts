import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-sidenav-body',
  templateUrl: './sidenav-body.component.html',
  styleUrls: ['./sidenav-body.component.scss']
})
export class SidenavBodyComponent implements OnInit {
  @Input() sidenav: any;
  public menuItems: [any] = [{
    name: 'NAVIGATION.HOME',
    href: 'home',
    child: false,
    icon: 'home',
    forAdmin: false
  }, {
    name: 'NAVIGATION.MATERIALS',
    href: 'materials',
    child: false,
    icon: 'gradient',
    forAdmin: false
  }, {
    name: 'NAVIGATION.PROCESSES',
    href: 'processes',
    child: false,
    icon: 'tune',
    forAdmin: false
  }, {
    name: 'NAVIGATION.SETTINGS',
    href: 'settings',
    child: false,
    icon: 'settings',
    forAdmin: true
  }];

  constructor() {
  }

  ngOnInit() {
  }

}
