import {Component, OnInit} from '@angular/core';
import {MaterialsService} from "../../services/materials.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-materials',
  templateUrl: './materials.component.html',
  styleUrls: ['./materials.component.scss']
})
export class MaterialsComponent implements OnInit {
  public materials = [];
  public selectionCounter = 0;

  constructor(private materialsService: MaterialsService, private router: Router) {
  }

  public getMaterials() {
    this.materialsService.getMaterials().then((response) => {
      this.materials = response;
      this.selectionCounter = 0;
      // if (response.length === 0) {
      //   toast.show($filter('translate')('TOAST_NO_DATA'));
      // }
    });
  };

  // TODO change to removing all materials by one query
  public removeMaterial(id) {
    this.materialsService.deleteMaterial(id).then((response) => {
      this.getMaterials();
      this.selectionCounter = 0;
      this.router.navigate(['material']);
      // toast.show($filter('translate')('TOAST_DELETE_SUCCESS'));
    });
  };

  // $scope.$on('someMaterialUpadated', function () {
  //   getMaterials();
  // });

  public openAdditionDialog(event) {
    // $mdDialog.show({
    //   controller: 'materialAdditionDialogCtrl',
    //   templateUrl: 'templates/materialAdditionDialog.tmpl.html',
    //   parent: angular.element(document.body),
    //   targetEvent: event,
    //   clickOutsideToClose: true,
    //   openFrom: '#addButton', //TODO fix it
    //   fullscreen: true
    // }).then(function () {
    //   getMaterials();
    // }, function () {
    // });
  };

  public removeListItem() {
    this.materials.forEach((material) => {
      if (material.selected) {
        this.removeMaterial(material._id);
      }
    })
  };

  private checkSelectionCounter(material) {
    if (material.selected) {
      this.selectionCounter++;
    } else {
      this.selectionCounter--;
    }
  };


  ngOnInit() {
    this.getMaterials();
  }
}
