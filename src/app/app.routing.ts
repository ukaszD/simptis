import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {HomeComponent} from "./components/home/home.component";
import {SettingsComponent} from "./components/settings/settings.component";
import {MaterialsComponent} from "./components/materials/materials.component";
import {ProcessesComponent} from "./components/processes/processes.component";
import {LoginComponent} from "./components/login/login.component";
import {MaterialDetailsComponent} from "./components/materials/material-details/material-details.component";
import {ProcessDetailsComponent} from "./components/process-details/process-details.component";
import {AuthGuard} from "./auth.guard";

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  }, {
    path: 'home',
    canActivate: [AuthGuard],
    component: HomeComponent
  }, {
    path: 'login',
    component: LoginComponent
  }, {
    path: 'settings',
    canActivate: [AuthGuard],
    component: SettingsComponent
  }, {
    path: 'materials',
    canActivate: [AuthGuard],
    component: MaterialsComponent,
    children: [
      {
        path: ':materialId',
        component: MaterialDetailsComponent
      }
    ]
  }, {
    path: 'processes',
    canActivate: [AuthGuard],
    component: ProcessesComponent,
    children: [
      {
        path: ':processId',
        component: ProcessDetailsComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule {}
