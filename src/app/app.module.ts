import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
/**THIRDPARTY_LIBRARIES*/
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {
  MdButtonModule,
  MdSidenavModule,
  MdToolbarModule,
  MdListModule,
  MdMenuModule, MdCardModule, MdGridListModule, MdInputModule, MdProgressSpinnerModule
} from '@angular/material';
import {ToastrModule} from "ngx-toastr";
/**COMPONENTS*/
import {AppComponent} from './app.component';
import {NavbarComponent} from './components/navbar/navbar.component';
import {SidenavBodyComponent} from './components/sidenav-body/sidenav-body.component';
import {Http, HttpModule, JsonpModule} from '@angular/http';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {HomeComponent} from './components/home/home.component';
import {MaterialsComponent} from './components/materials/materials.component';
import {ProcessesComponent} from './components/processes/processes.component';
import {SettingsComponent} from './components/settings/settings.component';
import {LoginComponent} from './components/login/login.component';
import {MaterialDetailsComponent} from './components/materials/material-details/material-details.component';
import {ProcessDetailsComponent} from './components/process-details/process-details.component';
import {AppRoutingModule} from "./app.routing";

/**SERVICES*/
import {ConfigService} from "./services/config.service";
import {RestService} from "./services/rest.service";
import {DataService} from "./services/data.service";
import {AuthenticationService} from "./services/authentication.service";
import {FormsModule} from "@angular/forms";
import {AuthGuard} from "./auth.guard";
import {MaterialsService} from "./services/materials.service";
import {ProcessesService} from "./services/processes.service";

export function httpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidenavBodyComponent,
    HomeComponent,
    MaterialsComponent,
    ProcessesComponent,
    SettingsComponent,
    LoginComponent,
    MaterialDetailsComponent,
    ProcessDetailsComponent
  ],
  imports: [
    HttpModule,
    FormsModule,
    HttpClientModule,
    JsonpModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: httpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    MdButtonModule,
    MdSidenavModule,
    MdToolbarModule,
    MdListModule,
    MdMenuModule,
    MdCardModule,
    MdGridListModule,
    MdInputModule,
    MdProgressSpinnerModule,
    ToastrModule.forRoot({positionClass: 'toast-bottom-right'})
  ],
  providers: [
    ConfigService,
    RestService,
    DataService,
    AuthenticationService,
    MaterialsService,
    ProcessesService,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
