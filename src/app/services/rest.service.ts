import {Injectable} from '@angular/core';
import {Headers, Http, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/toPromise';
import {environment} from '../../environments/environment';
import {ToastrService} from "ngx-toastr";
import {Router} from "@angular/router";
import {current} from "codelyzer/util/syntaxKind";

@Injectable()
export class RestService {
  private serverUrl = environment.API_URL + '/api/';
  private options = new RequestOptions();

  constructor(private http: Http, private toastrService: ToastrService, private router: Router) {
    this.options.headers = new Headers();
    let sessionCurrentUser: any = JSON.parse(localStorage.getItem('currentUser'));
    if (sessionCurrentUser) {
      this.clearAuthHeaders();
      this.setAuthHeaders(sessionCurrentUser.token);
    }
  }

  public setAuthHeaders(token: string) {
    this.options.headers.append('x-access-token', token);
    this.options.headers.append('Authorization', 'Bearer ' + token);
  }

  public clearAuthHeaders() {
    this.options.headers.delete('x-access-token');
    this.options.headers.delete('Authorization');
  }

  public get(url: string, queryParams: string = ''): Promise<any> {
    return new Promise((resolve, reject) => {
      this
        .http
        .get(this.serverUrl + url + queryParams, this.options)
        .toPromise()
        .then((response) => {
          resolve(response.json());
        })
        .catch(error => {
          this.handleError(error);
          reject(error);
        });
    });
  }

  public post(url: string, body: any, queryParams: string = ''): Promise<any> {
    return new Promise((resolve, reject) => {
      this
        .http
        .post(this.serverUrl + url + queryParams, body, this.options)
        .toPromise()
        .then((response) => {
          // TODO to refactor/remove
          // this
          //   .toastrService
          //   .success(this.serverUrl + url + queryParams, 'Post request - Operation success');
          resolve(response.json());
        })
        .catch(error => {
          this.handleError(error);
          reject(error);
        });
    });
  }

  public put(url: string, body: any, queryParams: string = ''): Promise<any> {
    return new Promise((resolve, reject) => {
      this
        .http
        .put(this.serverUrl + url + queryParams, body, this.options)
        .toPromise()
        .then((response) => {
          // TODO to refactor/remove
          // this
          //   .toastrService
          //   .success(this.serverUrl + url + queryParams, 'Put request - Operation success');
          resolve(response.json());
        })
        .catch(error => {
          this.handleError(error);
          reject(error);
        });
    });
  }

  public delete(url: string, queryParams: string = ''): Promise<any> {
    return new Promise((resolve, reject) => {
      this
        .http
        .delete(this.serverUrl + url + queryParams, this.options)
        .toPromise()
        .then((response) => {
          // TODO to refactor/remove
          // this
          //   .toastrService
          //   .warning(this.serverUrl + url + queryParams, 'Delete request - Operation success');
          resolve(response.json());
        })
        .catch(error => {
          this.handleError(error);
          reject(error);
        });
    });
  }

  private handleError(error: any): Promise<any> {
    this
      .toastrService
      .error(error.message, (error.status !== undefined)
        ? 'Server error occured! (' + error.status + ')'
        : 'Server error occured!');
    console.error('### HTTP SERVER ERROR OCCURED:', error);
    if (error.status === 403) {
      this.router.navigate(['login']);
    }
    return Promise.reject(error.message || error);
  }

}
