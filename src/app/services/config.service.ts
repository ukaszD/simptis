import {Injectable} from '@angular/core';
import {RestService} from "./rest.service";
import {Http} from "@angular/http";
import {DataService} from "./data.service";

@Injectable()
export class ConfigService {

  constructor(private http: Http, private dataService: DataService, private restService: RestService) {
  }

  public getAndSaveAppConfig() {
    return this.restService.get('config').then((res) => {
      this.dataService.admin = res.admin;
      this.dataService.appConfig = res;
    }, (err) => {
      console.warn(err);
      // toast.show($filter('translate')('TOAST_SERVER_ERROR') + ' HTTP Status (' + code + '): ' + err.error.message);
    });
  }

  public getAppConfig() {
    return this.restService.get('config');
  }

  public getAllUser() {
    return this.restService.get('user/all');
  }

  public getLoggedUser() {
    return this.restService.get('user?loggedUserId=' + this.dataService.loggedUser._id);
  }

  public createUser(user) {
    return this.restService.post('user', user);
  }

  public deleteUser(userId) {
    return this.restService.delete('user', userId);
  }

  public getChemicalElements() {
    return this.restService.get('chemicalelement');
  }

  public createChemicalElement(chemicalElement) {
    return this.restService.post('chemicalelement', chemicalElement);
  }

  public updateChemicalElement(elementId, element) {
    return this.restService.put('chemicalelement', elementId, element);
  }

  public deleteChemicalElement(elementId) {
    return this.restService.delete('chemicalelement', elementId);
  }
}
