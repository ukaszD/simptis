import {Injectable} from '@angular/core';
import {RestService} from "./rest.service";
import {DataService} from "./data.service";
import {Http} from "@angular/http";
import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core";

@Injectable()
export class AuthenticationService {
  private token: string = null;

  constructor(private restService: RestService, private dataService: DataService, private http: Http,
              private toastr: ToastrService, private translate: TranslateService) {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser.token) {
      this.token = currentUser.token;
    }
  }

  public login(username, password) {
    return new Promise((resolve, reject) => {
        this.restService.post('authenticate', {
          username: username,
          password: password
        }).then((response) => {
            if (response.token) {
              localStorage.currentUser = JSON.stringify({
                username: username,
                token: response.token,
                user: response.user
              });

              this.dataService.loggedUser = response.user;
              this.restService.setAuthHeaders(response.token);
              this.token = response.token;

              resolve({
                success: true,
                username: username,
                user: response.user
              });
              this.translate.get('TOASTR.SIGN_IN_SUCCESS').subscribe((text) => {
                this.toastr.success(text);
              })

            } else {
              reject({
                success: false
              });
              this.translate.get('TOASTR.SIGN_IN_ERROR').subscribe((text) => {
                this.toastr.success(text);
              })
            }
          }
        );
      }
    );
  };

  public getToken() {
    return this.token;
  }

  public logout = function () {
    delete localStorage.currentUser;
    this.restService.clearAuthHeaders();
    this.dataService.loggedUser = null;
    this.token = null;
    // $rootScope.$broadcast('loggedUserChange');
  };

  public isAuthenticated() {
    return JSON.parse(localStorage.getItem('currentUser')) ? true : false;
  };

}
