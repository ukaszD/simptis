import {Injectable} from '@angular/core';
import {RestService} from "./rest.service";
import {DataService} from "./data.service";
import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core";
import {Http, Request, ResponseContentType} from "@angular/http";

@Injectable()
export class ProcessesService {

  constructor(private http: Http, private rest: RestService, private dataService: DataService, private toastr: ToastrService, private translate: TranslateService) {
  }

  public getProcess(processId) {
    return this.rest.get('process/' + processId + '?loggedUserId=' + this.dataService.loggedUser._id);
  };

  public getProcesses() {
    return this.rest.get('process?loggedUserId=' + this.dataService.loggedUser._id);
  };

  public getProcessesCount() {
    return this.rest.get('count/processes');
  };

  public createProcess(process) {
    return this.rest.post('process?loggedUserId=' + this.dataService.loggedUser._id, process);
  };

  public updateProcess(processId, process) {
    return this.rest.put('process/' + processId + '?loggedUserId=' + this.dataService.loggedUser._id, null, process);
  };

  public deleteProcess(processId) {
    return this.rest.delete('process/' + processId + '?loggedUserId=' + this.dataService.loggedUser._id);
  };

  public readResultFile(processId) {
    return this.rest.get('process/' + processId + '/solver?loggedUserId=' + this.dataService.loggedUser._id);
  };

  public runCmd(processId) {
    return this.rest.post('process/' + processId + '/solver?loggedUserId=' + this.dataService.loggedUser._id, {
      socketId: this.dataService.userSockedId
    });
  };

  public killProcess(processId, pid) {
    return this.rest.delete('process/' + processId + '/solver?pid=' + pid + '&loggedUserId=' + this.dataService.loggedUser._id);
  };

  public getConsoleLogFile(processId) {
    return this.rest.get('process/' + processId + '/solver/consolelog?loggedUserId=' + this.dataService.loggedUser._id);
  };

  public createInputFile(processId, templateData) {
    return new Promise((resolve, reject) => {
      this.rest.post('process/' + processId + '/inputfile?loggedUserId=' + this.dataService.loggedUser._id, {
        templateData: templateData
      }).then(function (res) {
        resolve(res);
        this.translate.get('TOASTR.TOAST_CREATE_INPUTFILE_SUCCESS').subscribe((text) => {
          this.toastr.success(text);
        });
      }, (err) => {
        reject(err.data);
        if (err.code) {
          this.translate.get('TOASTR.TOAST_SERVER_ERROR').subscribe((text) => {
            this.toastr.success(text + ' HTTP Status (' + err.code + '): ' + err.error.message);
          });
        } else {
          let response = err.data.error.code ? (err.data.error.code + ' (when call ' + err.data.error.syscall + ')') : err.data.error.message;
          this.translate.get('TOASTR.TOAST_SERVER_ERROR').subscribe((text) => {
            this.toastr.success(text + ': ' + response);
          });
        }
      });
    })
  };

  public downloadResultsFiles(processId) {
    let req = new Request({
      url: 'process/' + processId + '/solver/results?loggedUserId=' + this.dataService.loggedUser._id,
      method: "GET",
      responseType: ResponseContentType.Blob
    });
    return this.http.request(req);
  };

  public loadInputFile(processId, hideToast) {
    return this.rest.get('process/' + processId + '/inputfile?loggedUserId=' + this.dataService.loggedUser._id);
  }
}
