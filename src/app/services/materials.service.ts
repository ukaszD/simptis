import {Injectable} from '@angular/core';
import {RestService} from "./rest.service";
import {ToastrService} from "ngx-toastr";
import {DataService} from "./data.service";

@Injectable()
export class MaterialsService {

  constructor(private rest: RestService, private toastr: ToastrService, private dataService: DataService) {
  }

  public getMaterials() {
    return this.rest.get('material?loggedUserId=' + this.dataService.loggedUser._id);
  };

  public getMaterial(materialId) {
    return this.rest.get('material/' + materialId + '?loggedUserId=' + this.dataService.loggedUser._id);
  };

  public getMaterialImageColors(materialId, imageFilePath) {
    return this.rest.get('material/' + materialId + '/getcolors?path=' + imageFilePath);
  };

  public getMaterialsCount() {
    return this.rest.get('count/materials');
  };

  public createMaterial(material) {
    return this.rest.post('material?loggedUserId=' + this.dataService.loggedUser._id, material);
  };

  public createMaterialWithImage(body, file) {
    return new Promise((resolve, reject) => {
      // this.rest.post('material?loggedUserId=' + this.dataService.loggedUser._id, {
      //   data: {
      //     file: file,
      //     materialContent: body
      //   }
      // }).then((response) => {
      //   resolve(response);
      //   this.toastr.success('TOAST_CREATE_SUCCESS' + ' ' + response.data.name);
      // }, (err) => {
      //   reject(err);
      //   this.toastr.error('TOAST_SERVER_ERROR' + '. ' + err.error.message);
      // });
    });
  };

  public updateMaterialWithImage(materialId, body, file) {
    return new Promise((resolve, reject) => {
      // Upload.upload({
      //   url: 'material/' + materialId + '?loggedUserId=' + this.dataService.loggedUser._id,
      //   method: 'PUT',
      //   data: {
      //     file: file,
      //     materialContent: body
      //   }
      // }).then((response) => {
      //   resolve(response);
      //   this.toastr.success('TOAST_UPDATE_SUCCESS' + ' ' + response.data.name);
      // }, (err, code) => {
      //   reject(err);
      //   this.toastr.error('TOAST_SERVER_ERROR' + '. ' + err.data.error.message);
      // })
    });

  };

  public updateMaterial(materialId, material) {
    return this.rest.put('material/' + materialId + '?loggedUserId=' + this.dataService.loggedUser._id, null, material);
  };

  public deleteMaterial(materialId) {
    return this.rest.delete('material/' + materialId + '?loggedUserId=' + this.dataService.loggedUser._id);
  };

}
