import {Injectable} from '@angular/core';

@Injectable()
export class DataService {
  public appConfig: any = {};
  public admin: any = {};
  public loggedUser: any = {};
  public userSockedId = null;

  constructor() {
    this.getLoggedUserData();
  }

  public getLoggedUserData() {
    let currentLocalStorageData = JSON.parse(localStorage.getItem('currentUser'));
    if (currentLocalStorageData) {
      this.loggedUser = currentLocalStorageData.user;
    }
  }
}
