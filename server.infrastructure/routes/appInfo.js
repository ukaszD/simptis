let AppConfig = require('../models/AppConfig.js');

module.exports = function (router) {
  router.route('/config')
    .get(function (req, res) {
      AppConfig.findOne({name: 'app_config'}, function (error, config) {
        if (error) {
          res.status(404).json({
            auth: true,
            error: error
          });
        }
        if (!config) {
          res.status(403).json({
            auth: true,
            error: error
          });
        } else if (config) {
          config.lastLoggedUser = global.lastLoggedUser;
          res.status(200).json(config);
        }
      });
    })
};
