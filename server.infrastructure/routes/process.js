var Process = require('../models/Process.js');
var Material = require('../models/Material.js');
var spawn = require('child_process').spawn;
var q = require('q');

module.exports = function (router, handlebars, fs, config, io, archiver) {
    router.route('/process')
        .get(function (req, res) {
            var queryParams = req.query;
            Process.find({
                owner: queryParams.loggedUserId
            }).populate('material').populate({
                path: 'owner',
                select: '_id username'
            }).exec(function (error, result) {
                if (error)
                    res.status(404).json({
                        auth: true,
                        error: error
                    });
                else {
                    res.status(200).json(result);
                }
            });
        })
        .post(function (req, res) {
            var body = req.body;
            var queryParams = req.query;
            body.owner = queryParams.loggedUserId;
            var newProcess = new Process(body);
            newProcess.save(function (error, result) {
                if (error)
                    res.status(404).json({
                        auth: true,
                        error: error
                    });
                else
                    res.status(200).json(result);
            });
        });

    router.route('/process/:id')
        .get(function (req, res) {
            var reqParams = req.params;
            var queryParams = req.query;
            Process.findOne({
                _id: reqParams.id,
                owner: queryParams.loggedUserId
            }).populate('material').populate({
                path: 'owner',
                select: '_id username'
            }).exec(function (error, result) {
                if (error)
                    res.status(404).json({
                        auth: true,
                        error: error
                    });
                else {
                    if (result.material) {
                        if (result.material.structureImage) {
                            if (result.material.structureImage.data) {
                                var thumb = new Buffer(result.material.structureImage.data.buffer).toString('base64');
                                result.material.structureImage.convertedData = thumb;
                            }
                        }
                    }
                    res.status(200).json(result);
                }
            });
        })
        .put(function (req, res) {
            var body = req.body;
            var reqParams = req.params;
            Process.findByIdAndUpdate(reqParams.id, body, function (error, result) {
                if (error)
                    res.status(404).json({
                        auth: true,
                        error: error
                    });
                else
                    res.status(200).json(result);
            });
        })
        .delete(function (req, res) {
            var reqParams = req.params;
            var queryParams = req.query;
            Process.remove({
                _id: reqParams.id,
                owner: queryParams.loggedUserId
            }, function (error, result) {
                if (error) {
                    res.status(400).json({
                        auth: true,
                        error: error
                    });
                } else {
                    var unusedDir = '.' + config.dirPaths.app_data.processes + '/' + queryParams.loggedUserId + '/' + reqParams.id + '/';

                    fs.readdir(unusedDir + '/', function(err, filenames) {
                        if (err) {
                            res.status(200).json({
                                auth: true,
                                error: {
                                    message: 'Dir not found',
                                    code: err.code,
                                    errno: err.errno,
                                    syscall: err.syscall,
                                    path: err.path
                                }
                            });
                        } else {
                            var i = filenames.length;
                            if (i === 0) {
                                fs.rmdir(unusedDir, function (err) {
                                    if (err) {
                                        res.status(204).json({
                                            auth: true,
                                            error: err
                                        });
                                    } else {
                                        res.status(200).json({
                                            status: 1,
                                            message: "Process files deleted permanently"
                                        });
                                    }
                                });
                            }
                            filenames.forEach(function (filename) {
                                fs.unlink(unusedDir + filename, function (error) {
                                    if (error) {
                                        console.log('Failed to delete unused process file' + filename);
                                        res.status(204).json({
                                            auth: true,
                                            error: error
                                        });
                                    } else {
                                        if (i === 1) {
                                            fs.rmdir(unusedDir, function (e) {
                                                if (e) {
                                                    res.status(204).json({
                                                        auth: true,
                                                        error: e
                                                    });
                                                } else {
                                                    res.status(200).json({
                                                        status: 1,
                                                        message: "Process files deleted permanently"
                                                    });
                                                }
                                            });
                                        }
                                    }
                                    i--;
                                });
                            });

                        }
                    });
                }
            });
        });

    router.route('/process/:id/inputfile')
        .get(function (req, res) {
            var queryParams = req.query;
            var params = req.params;
            var path = '.' + config.dirPaths.app_data.processes + '/' + queryParams.loggedUserId + '/' + params.id + '/' + config.inputFile.name;
            fs.readFile(path, 'utf-8', function (err, file) {
                if (err) {
                    res.status(204).json({
                        auth: true,
                        error: err.code === 'ENOENT' ? {
                            message: 'Input file not found'
                        } : err
                    });
                } else {
                    res.status(200).json(file.toString());
                }
            });
        })
        .post(function (req, res) {
            var body = req.body;
            var queryParams = req.query;
            fs.readFile(config.inputFile.templatePath, 'utf-8', function (err, source) {
                if (err) {
                    res.status(404).json({
                        auth: true,
                        error: err
                    });
                } else {
                    //Creating SolverInputFile.sim with handlebars and input.tmpl.html template
                    var template = handlebars.compile(source);
                    var context = body.templateData;
                    if (!context.material) {
                        res.status(404).json({
                            auth: true,
                            error: {
                                message: 'Material in process not found'
                            }
                        });
                    } else {
                        var compiledTemlate = template(context);
                        var destinationPathDir = '.' + config.dirPaths.app_data.processes;
                        //Making new dir in config.dirPaths.app_data.processes dir
                        fs.mkdir(destinationPathDir, function (err) {
                            if (!err || err.code === 'EEXIST') {
                                destinationPathDir += ('/' + queryParams.loggedUserId);
                                fs.mkdir(destinationPathDir, function (err) {
                                    if (!err || err.code === 'EEXIST') {
                                        destinationPathDir += ('/' + context._id);
                                        fs.mkdir(destinationPathDir, function (err) {
                                            if (!err || err.code === 'EEXIST') {
                                                //Creating new file from compiled input file template
                                                fs.writeFile(destinationPathDir + '/' + config.inputFile.name, compiledTemlate, function (err) {
                                                    if (err) {
                                                        res.status(400).json({
                                                            auth: true,
                                                            error: err
                                                        });
                                                    } else {
                                                        res.status(200).json({
                                                            status: 'OK'
                                                        });
                                                    }
                                                });
                                            } else {
                                                res.status(400).json({
                                                    auth: true,
                                                    error: err
                                                });
                                            }
                                        });
                                    } else {
                                        res.status(400).json({
                                            auth: true,
                                            error: err
                                        });
                                    }
                                });
                            } else {
                                res.status(400).json({
                                    auth: true,
                                    error: err
                                });
                            }
                        });
                    }
                }
            });
        });

    router.route('/process/:id/solver')
        .get(function (req, res) {
            var reqParams = req.params;
            var queryParams = req.query;
            var filePath = '.' + config.dirPaths.app_data.processes + '/' + queryParams.loggedUserId + '/' + reqParams.id + '/' + config.outputFile.name;
            fs.readFile(filePath, 'utf8', function (err, file) {
                if (err) {
                    res.status(400).json({
                        auth: true,
                        error: {
                            message: 'File not found',
                            code: err.code,
                            errno: err.errno,
                            syscall: err.syscall,
                            path: err.path
                        }
                    });
                } else {
                    var fileData = {
                        No: [],
                        Time: [],
                        Temperature: [],
                        FerriteV: [],
                        AusteniteV: [],
                        AusteniteC: [],
                        FerriteFraction: [],
                        AusteniteFraction: [],
                        DCoefficient: [],
                        AvgCConc: [],
                        HeatingRate: [],
                        Nucleation: []
                    };
                    var rows = file.toString().split('\n');
                    var length = rows.length;
                    for (var i = 1; i < length - 1; i++) {
                        var splited = rows[i].slice(0, -1).split('\t');
                        fileData.No.push(splited[0]);
                        fileData.Time.push(splited[1]);
                        fileData.Temperature.push(splited[2]);
                        fileData.FerriteV.push(splited[3]);
                        fileData.AusteniteV.push(splited[4]);
                        fileData.AusteniteC.push(splited[5]);
                        fileData.FerriteFraction.push(splited[6]);
                        fileData.AusteniteFraction.push(splited[7]);
                        fileData.DCoefficient.push(splited[8]);
                        fileData.AvgCConc.push(splited[9]);
                        fileData.HeatingRate.push(splited[10]);
                        fileData.Nucleation.push(splited[11]);
                    }
                    res.status(200).json(fileData);
                }
            });
        })
        .post(function (req, res) {
            var body = req.body;
            var reqParams = req.params;
            var queryParams = req.query;

            var startSolver = function () {
                var path = '.' + config.dirPaths.app_data.processes + '/' + queryParams.loggedUserId + '/' + reqParams.id + '/';

                Process.findById(reqParams.id, function (error, findedProcess) {
                    if (error)
                        res.status(404).json({
                            auth: true,
                            error: error
                        });
                    else {
                        if (findedProcess.pid === -1) {
                            var errorWithConsoleLog = null;
                            //create console.log file
                            fs.writeFile(path + '/console.log', '', function (err) {
                                if (err) {
                                    errorWithConsoleLog = true;
                                    res.status(404).json({
                                        auth: true,
                                        error: err
                                    });
                                } else {
                                    console.log("Console log file was created!");
                                }
                            });

                            //emit 'start' to all connected socket
                            io.sockets.in(findedProcess.owner.toString()).emit(findedProcess._id, {
                                stdout: 'Script started...'
                            });

                            //create new child process - run solver script
                            var childProcess = spawn('bash', [config.solver.scriptPath], {
                                cwd: path,
                                detached: true
                            });
                            console.log('Spawned child pid: ' + childProcess.pid);
                            Process.update({
                                _id: findedProcess._id
                            }, {
                                pid: childProcess.pid === undefined ? -1 : childProcess.pid
                            }, function (error, updatedItem) {
                                if (error) {
                                    res.status(404).json({
                                        auth: true,
                                        error: error
                                    });
                                } else {
                                    if (!errorWithConsoleLog) {
                                        res.status(200).json({
                                            message: 'Child process pid saved',
                                            processId: childProcess.pid === undefined ? -1 : childProcess.pid,
                                            updatedItem: updatedItem
                                        });
                                    }
                                }
                            });

                            childProcess.stdout.on('data', function (data) {
                                if (data) {
                                    writeConsoleLogFile(path + '/console.log', data.toString(), findedProcess, io);
                                }
                            });

                            childProcess.stderr.on('data', function (data) {
                                if (data) {
                                    writeConsoleLogFile(path + '/console.log', data.toString(), findedProcess, io);
                                }
                            });

                            childProcess.on('exit', function (code) {
                                if (code) {
                                    writeConsoleLogFile(path + '/console.log', 'exit with code: ' + code.toString(), findedProcess, io);
                                    Process.update({
                                        _id: findedProcess._id
                                    }, {
                                        pid: -1
                                    }, function (error, updatedItem) {
                                        if (error) {
                                            res.status(404).json({
                                                auth: true,
                                                error: error
                                            });
                                        } else {
                                            console.log('Child process id deleted');
                                        }
                                    });
                                }
                            });

                            childProcess.on('error', function (err) {
                                if (err) {
                                    writeConsoleLogFile(path + '/' + config.solver.consoleLogFile, 'error: ' + err.toString(), findedProcess, io);
                                    Process.update({
                                        _id: findedProcess._id
                                    }, {
                                        pid: -1
                                    }, function (error, updatedItem) {
                                        if (error) {
                                            res.status(404).json({
                                                auth: true,
                                                error: error
                                            });
                                        } else {
                                            console.log('Child process id deleted');
                                        }
                                    });
                                }
                            });
                        } else {
                            res.status(404).json({
                                auth: true,
                                error: {
                                    message: 'Solver still running, firstly kill old process'
                                }
                            });
                        }
                    }
                });
            };
            startSolver();
        })
        .delete(function (req, res) {
            var reqParams = req.params;
            var queryParams = req.query;

            Process.findById(reqParams.id, function (error, findedProcess) {
                if (error)
                    res.status(404).json({
                        auth: true,
                        error: error
                    });
                else {
                    if (findedProcess.pid !== -1) {
                        Process.update({
                            _id: findedProcess._id
                        }, {
                            pid: -1
                        }, function (err, updatedItem) {
                            if (error) {
                                res.status(404).json({
                                    auth: true,
                                    error: err
                                });
                            } else {
                                res.status(200).json({
                                    message: 'Process killed',
                                    processId: queryParams.pid
                                });
                            }
                        });
                        //TODO to repair
                        process.kill(-findedProcess.pid);
                        console.log('Process ' + queryParams.pid + ' killed');
                    } else {
                        res.status(404).json({
                            auth: true,
                            error: {
                                message: 'Process pid = -1'
                            }
                        });
                    }
                }
            });

        });

    router.route('/process/:id/solver/consolelog').get(function (req, res) {
        var reqParams = req.params;
        var queryParams = req.query;
        var filePath = '.' + config.dirPaths.app_data.processes + '/' + queryParams.loggedUserId + '/' + reqParams.id + '/' + config.solver.consoleLogFile;
        fs.readFile(filePath, 'utf8', function (err, file) {
            if (err) {
                res.status(204).json({
                    auth: true,
                    error: {
                        message: 'File not found',
                        code: err.code,
                        errno: err.errno,
                        syscall: err.syscall,
                        path: err.path
                    }
                });
            } else {
                var fileData = [];
                var rows = file.toString().split('\n');
                var length = rows.length;
                for (var i = 0; i < length; i++) {
                    var splited = rows[i].slice(0, -1).split('\t');
                    fileData.push({
                        time: splited[0] + ' ' + splited[1],
                        std: splited[2],
                        timestamp: new Date().getTime()
                    });
                }
                res.status(200).json(fileData);
            }
        });
    });

    router.route('/process/:id/solver/results').get(function (req, res) {
        var reqParams = req.params;
        var queryParams = req.query;
        var dirPath = '.' + config.dirPaths.app_data.processes + '/' + queryParams.loggedUserId + '/' + reqParams.id + '/';

        fs.readdir(dirPath, function(err, filenames) {
            if (err) {
                res.status(400).json({
                    auth: true,
                    error: {
                        message: 'Dir not found',
                        code: err.code,
                        errno: err.errno,
                        syscall: err.syscall,
                        path: err.path
                    }
                });
            } else {
                var output = fs.createWriteStream(dirPath + config.solver.resultZipArchiveName);
                var archive = archiver('zip', {
                    store: true
                });

                output.on('close', function() {
                    console.log('results.zip created. ' + archive.pointer() + ' total bytes');
                    var file = fs.readFileSync(dirPath + config.solver.resultZipArchiveName, 'binary');
                    res.setHeader('Content-Type', 'application/zip');
                    res.setHeader('Content-Disposition', reqParams.id  + '_' + config.solver.resultZipArchiveName);
                    fs.unlink(dirPath + config.solver.resultZipArchiveName, function (err)  {
                        if (err) {
                            res.status(400).json({
                                auth: true,
                                error: {
                                    message: 'results.zip not found',
                                    code: err.code,
                                    errno: err.errno,
                                    syscall: err.syscall,
                                    path: err.path
                                }
                            });
                        } else {
                            console.log('results.zip successfully deleted');
                        }
                    });
                    res.end(file, 'binary');

                });
                archive.on('error', function(err) {
                    res.status(400).json({
                        auth: true,
                        error: {
                            message: 'Archiver error',
                            code: err.code,
                            errno: err.errno,
                            syscall: err.syscall,
                            path: err.path
                        }
                    });
                });
                archive.pipe(output);
                filenames.forEach(function(filename) {
                    archive.file(dirPath + filename, { name: filename });
                });
                archive.finalize();


            }
        });       
    });

      
    router.route('/count/processes')
        .get(function (req, res) {
            Process.count({}, function (error, result) {
                if (error)
                    res.status(404).json({
                        auth: true,
                        error: error
                    });
                else {
                    res.status(200).json(result);
                }
            });
        });

    var writeConsoleLogFile = function (path, data, process) {
        var currentDate = new Date();
        var date = currentDate.getDate() + '.' + ((currentDate.getMonth() + 1) < 10 ? '0' + (currentDate.getMonth() + 1) : (currentDate.getMonth() + 1)) + '.' + currentDate.getFullYear() + '\t' + ((currentDate.getHours()) < 10 ? '0' + (currentDate.getHours()) : (currentDate.getHours())) + ':' + ((currentDate.getMinutes()) < 10 ? '0' + (currentDate.getMinutes()) : (currentDate.getMinutes())) + ':' + ((currentDate.getSeconds()) < 10 ? '0' + (currentDate.getSeconds()) : (currentDate.getSeconds())) + '\t';
        fs.appendFile(path, date + data, function (err) {
            if (err) {
                //console.log(err);
                io.sockets.in(process.owner).emit(process._id.toString(), {
                    stdout: err
                });
            }
            //console.log(process.owner.toString(), process._id.toString());
            io.sockets.in(process.owner).emit(process._id.toString(), {
                stdout: data
            });
            //console.log("Console log file was saved!");
        });
    };
};