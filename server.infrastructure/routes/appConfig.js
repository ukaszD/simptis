let AppConfig = require('../models/AppConfig.js');
let User = require('../models/User.js');

module.exports = function (router, bcrypt) {
  //TODO to remove?
  router.route('/config')
    .post(function (req, res) {
      let body = req.body;
      let newAppConfig = new AppConfig(body);
      newAppConfig.save(function (error, result) {
        if (error)
          res.status(404).json({
            auth: true,
            error: error
          });
        else
          res.status(200).json(result);
      });
    });

  router.route('/user')
    .get(function (req, res) {
      var queryParams = req.query;
      User.findById(queryParams.loggedUserId).select('username role createAccountDate').exec(function (error, result) {
        if (error)
          res.status(404).json({
            auth: true,
            error: error
          });
        else {
          res.status(200).json(result);
        }
      });
    })
    .post(function (req, res) {
      let body = req.body;
      bcrypt.genSalt(10, function (err, salt) {
        bcrypt.hash(body.password, salt, function (err, hash) {
          body.password = hash;
          var newUser = new User(body);
          newUser.save(function (error, result) {
            if (error)
              res.status(404).json({
                auth: true,
                error: error
              });
            else
              res.status(200).json(result);
          });
        });
      });
    });

  router.route('/user/all')
    .get(function (req, res) {
      User.find({}).select('username role createAccountDate').exec(function (error, result) {
        if (error)
          res.status(404).json({
            auth: true,
            error: error
          });
        else {
          res.status(200).json(result);
        }
      });
    });

  router.route('/user/:id')
    .delete(function (req, res) {
      let params = req.params;
      User.remove({_id: params.id}).exec(function (error, result) {
        if (error)
          res.status(404).json({
            auth: true,
            error: error
          });
        else {
          res.status(200).json(result);
        }
      });
    })
};
