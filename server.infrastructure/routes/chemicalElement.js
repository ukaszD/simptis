var ChemicalElement = require('../models/ChemicalElement.js');

module.exports = function(router) {
    router.route('/chemicalelement')
        .get(function(req, res) {
            ChemicalElement.find({}, null, {sort:{name: 1}},function(error, result) {
                if (error)
                    res.status(404).json({
                        auth: true,
                        error: error
                    });
                else
                    res.status(200).json(result);
            });
        })
        .post(function(req, res) {
            var body = req.body;
            var newChemicalElement = new ChemicalElement(body);
            newChemicalElement.save(function(error, result) {
                if (error)
                    res.status(404).json({
                        auth: true,
                        error: error
                    });
                else
                    res.status(200).json(result);
            });
        });

    router.route('/chemicalelement/:id')
        .get(function(req, res) {
            var reqParams = req.params;
            ChemicalElement.findById(reqParams.id, function(error, result) {
                if (error)
                    res.status(404).json({
                        auth: true,
                        error: error
                    });
                else
                    res.status(200).json(result);
            });
        })
        .put(function(req, res) {
            var body = req.body;
            var reqParams = req.params;
            ChemicalElement.findByIdAndUpdate(reqParams.id, body, function(error, result) {
                if (error)
                    res.status(404).json({
                        auth: true,
                        error: error
                    });
                else
                    res.status(200).json(result);
            });
        })
        .delete(function(req, res) {
            var reqParams = req.params;
            ChemicalElement.remove({
                _id: reqParams.id
            }, function(error, result) {
                if (error)
                    res.status(400).json({
                        auth: true,
                        error: error
                    });
                else
                    res.status(200).json(result);
            });
        });
};
