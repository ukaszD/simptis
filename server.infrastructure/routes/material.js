var Material = require('../models/Material.js');
var multiparty = require('connect-multiparty');
var multipartyMiddleware = multiparty();
var getPixels = require("get-pixels");

module.exports = function (router, fs, config, jwt) {
    router.route('/material')
        .get(function (req, res) {
            Material.find({}).populate({
                path: 'owner',
                select: '_id username'
            }).select('name owner description structureImage').exec(function (error, result) {
                if (error)
                    res.status(404).json({
                        auth: true,
                        error: error
                    });
                else {
                    res.status(200).json(result);
                }
            });
        })
        .post(multipartyMiddleware, function (req, res) {
            var queryParams = req.query;
            if (req.files) {
                var structureImageFile = req.files.file;
                var body = req.body.materialContent;
                console.log('Image ' + structureImageFile.name + ' passed to serwer!');
            } else {
                var body = req.body;
            }
            body.owner = queryParams.loggedUserId;
            var newMaterial = new Material(body);
            var timeTick = new Date().getTime();
            newMaterial.save({
                imageFile: structureImageFile,
                ownerId: queryParams.loggedUserId,
                timeTick: timeTick
            }, function (error, result) {
                if (error)
                    res.status(404).json({
                        auth: true,
                        error: error
                    });
                else
                    res.status(200).json(result);
            });
        });

    router.route('/materials')
        .get(function (req, res) {
            Material.find({}).populate({
                path: 'owner',
                select: '_id username'
            }).select('name description _id').exec(function (error, result) {
                if (error)
                    res.status(404).json({
                        auth: true,
                        error: error
                    });
                else {
                    res.status(200).json(result);
                }
            });
        });

    router.route('/material/:id')
        .get(function (req, res) {
            var reqParams = req.params;
            Material.findById({
                _id: reqParams.id,
            }).populate({
                path: 'owner',
                select: '_id username'
            }).exec(function (error, result) {
                if (error)
                    res.status(404).json({
                        auth: true,
                        error: error
                    });
                else {
                    if (result) {
                        res.status(200).json(result);
                    } else {
                        res.status(404).json({
                            auth: true,
                            error: {
                                message: 'Not found'
                            }
                        });
                    }
                }
            });
        })
        .put(multipartyMiddleware, function (req, res) {
            var reqUser = req.user;
            var materialId =  null;
            if (req.files) {
                materialId =  req.body.materialContent._id;
            } else {
                materialId =  req.body._id;
            }
            //authorization
            Material.findById(materialId).exec(function (error, item) {
                if (error) {
                    res.status(404).json({
                        auth: true,
                        error: error
                    });
                }
                console.log(item.owner.toString());
                if ((item.owner.toString() !== reqUser._id) && (reqUser.role !== 'ADMIN')) {
                    res.status(403).json({
                        auth: true,
                        error: {
                            message: 'Unauthorized operation'
                        }
                    });
                } else {
                    if (req.files) {
                        var structureImageFile = req.files.file;
                        var body = req.body.materialContent;
                        console.log('Image ' + structureImageFile.name + ' passed to serwer!');

                        //Manual data setting and using save middleware to update working properly!
                        item.name = body.name;
                        item.description = body.description ? body.description : '';
                        item.chemicalIngredients = body.chemicalIngredients;
                        item.balanceChart = body.balanceChart;
                        item.diffusionCoefficient = body.diffusionCoefficient;
                        var destinationPath = item.structureImage.path;

                        //Deleting old file from server directory
                        //TODO move to material pre save middleware if it is possible
                        fs.unlink(destinationPath, function (err) {
                            if (err && err.code !== 'ENOENT' ) {
                                console.log('Failed to delete temporary file: ' + destinationPath);
                                res.status(400).json({
                                    auth: true,
                                    error: err
                                });
                            } else {
                                console.log('Successfully deleted temporary file: ' + destinationPath);
                                var timeTick = new Date().getTime();
                                item.save({
                                    imageFile: structureImageFile,
                                    ownerId: item.owner.toString(),
                                    timeTick: timeTick
                                }, function (error, updatedItem) {
                                    if (error) {
                                        res.status(404).json({
                                            auth: true, error: error
                                        });
                                    } else {
                                        res.status(200).json({name: updatedItem.name});
                                    }
                                });
                            }
                        });
                    } else {
                        var body = req.body;
                        var reqParams = req.params;
                        Material.findByIdAndUpdate(reqParams.id, {
                            name: body.name,
                            description: body.description ? body.description : '',
                            chemicalIngredients: body.chemicalIngredients,
                            balanceChart: body.balanceChart,
                            diffusionCoefficient: body.diffusionCoefficient,
                            structureImage: body.structureImage
                        }, function (error, result) {
                            if (error)
                                res.status(404).json({
                                    auth: true,
                                    error: error
                                });
                            else
                                res.status(200).json(result);
                        });
                    }
                }
            });
        })
        .delete(function (req, res) {
            var reqParams = req.params;
            var queryParams = req.query;
            var reqUser = req.user;
            Material.findById(reqParams.id).exec(function (error, item) {
                if (error) {
                    res.status(404).json({
                        auth: true,
                        error: error
                    });
                }
                console.log(item.owner.toString());
                if ((item.owner.toString() !== reqUser._id) && (reqUser.role !== 'ADMIN')) {
                    res.status(403).json({
                        auth: true,
                        error: {
                            message: 'Unauthorized operation'
                        }
                    });
                } else {
                    item.remove(function(error) {
                        if (error)
                            res.status(400).json({
                                auth: true,
                                error: error
                            });
                        else {
                            //Deleting old file from server directory
                            var destinationPath = item.structureImage.path;
                            var unusedDir = '.' + config.dirPaths.app_data.materials + '/' + item.owner.toString() + '/' + item._id;
                            if (item.structureImage.path !== '') {
                                fs.unlink(destinationPath, function (err) {
                                    if (err) {
                                        console.log('Failed to delete temporary file: ' + destinationPath);
                                        res.status(204).json({
                                            auth: true,
                                            error: err
                                        });
                                    } else {
                                        console.log('Successfully deleted temporary file: ' + destinationPath);
                                        fs.rmdir(unusedDir, function (err) {
                                            if (err) {
                                                res.status(204).json({
                                                    auth: true,
                                                    error: err
                                                });
                                            } else {
                                                res.status(200).json({
                                                    status: "OK",
                                                    message: "Material deleted permanently"
                                                });
                                            }
                                        });
                                    }
                                });
                            } else {
                                res.status(200).json(item);
                            }
                        }
                    });
                }
            });
        });

    router.route('/count/materials')
        .get(function (req, res) {
            Material.count({}, function (error, result) {
                if (error)
                    res.status(404).json({
                        auth: true,
                        error: error
                    });
                else {
                    res.status(200).json(result);
                }
            });
        });

    router.route('/material/:materialId/getcolors')
        .get(function (req, res) {
            var reqQuery = req.query;

            function rgbToHex(r, g, b) {
                return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
            }

            function getPixel(x, y, pixels) {
                var out = [];
                var pointer = pixels.offset + (pixels.stride[0] * x) + (pixels.stride[1] * y);
                for (var i = 0; i < 4; i++) {
                    out.push(pixels.data[pointer + (pixels.stride[2] * i)]);
                }
                return {
                    rgba: {
                        separatelyChanels: out,
                        cssColor: 'rgba(' + out[0] + ',' + out[1] + ',' + out[2] + ',' + out[3] + ')'
                    },
                    hex: rgbToHex(out[0], out[1], out[2])
                };
            }

            if (reqQuery.path) {
                getPixels(reqQuery.path, function (err, pixels) {
                    if (err) {
                        res.status(404).json({auth: true, error: err});
                    } else {
                        var allPixels = [];
                        var colorsDictionary = {};
                        var colorsAmount = 0;
                        var size = pixels.shape.slice();
                        for (var i = 0; i < size[0]; i++) {
                            for (var j = 0; j < size[1]; j++) {
                                var pixel = getPixel(i, j, pixels);
                                allPixels.push(pixel);
                                if (colorsDictionary[pixel.hex] === undefined) {
                                    colorsDictionary[pixel.hex] = 1;
                                    colorsAmount++;
                                } else {
                                    colorsDictionary[pixel.hex]++;
                                }
                            }
                        }
                        var colors = [];
                        for (var color in colorsDictionary) {
                            colors.push({hex: color, amount: colorsDictionary[color]});
                        }

                        res.status(200).json({colors: colors, colorsAmount: colorsAmount, pixels: allPixels});
                    }
                });
            } else {
                res.status(404).json({auth: true, error: {message: 'Empty path'}});
            }
        });
};
