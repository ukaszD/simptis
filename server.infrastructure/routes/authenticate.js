var User = require('../models/User.js');

module.exports = function (router, app, jwt, bcrypt, io) {
    router.route('/authenticate')
        .post(function (req, res) {
            var body = req.body;
            User.findOne({username: body.username}).lean().exec(function (error, user) {
                if (error) {
                    res.status(404).json(error);
                }
                if (!user) {
                    res.status(403).json({
                        auth: false,
                        success: false,
                        error: {message: 'Authentication failed'}
                    });
                } else if (user) {
                    // check if password matches
                    bcrypt.compare(req.body.password, user.password, function (err, authenticated) {
                        if (authenticated) {
                            var token = jwt.sign(user, app.get('secret'), {
                                expiresIn: '72h' // expires in xx hours
                            });

                            // return the information including token as JSON
                            res.status(200).json({
                                auth: true,
                                username: user.username,
                                success: true,
                                message: 'Authentication success',
                                token: token,
                                user: { role: user.role, username: user.username, _id: user._id}
                            });
                            global.lastLoggedUser = { user: user.username, date: new Date()};
                        } else {
                            res.status(403).json({
                                auth: false,
                                success: false,
                                error: {message: 'Authentication failed'}
                            });
                        }
                    });
                }
            });
        });

    //Function for require token in all routes without authtentication and config
    router.use(function (req, res, next) {
        var token = req.body.token || req.query.token || req.headers['x-access-token'];
        if (token) {
            jwt.verify(token, app.get('secret'), function (err, decoded) {
                if (err) {
                    return res.status(403).json({auth: false, error: { message:'Failed to authenticate token.'}});
                } else {
                    req.user = decoded;
                    next();
                }
            });
        } else {
            return res.status(403).send({auth: false, error: {message: 'No token provided.'}});
        }
    });
};
