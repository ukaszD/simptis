let Material = require('../models/Material.js');
let multiparty = require('connect-multiparty');
let multipartyMiddleware = multiparty();
let getPixels = require("get-pixels");

let getAllMaterials = function () {
  return new Promise(function (resolve, reject) {
    Material.find({}).populate({
      path: 'owner',
      select: '_id username'
    }).select('name owner description structureImage').exec(function (error, result) {
      if (error)
        reject({
          auth: true,
          error: error
        });
      else {
        resolve(result);
      }
    });
  });
};


module.exports = {
  getAllMaterials: getAllMaterials
};
