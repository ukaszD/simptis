var uuid = require('node-uuid');

module.exports = {
    secret: uuid.v4(),
    dbUrls: {
        local: 'mongodb://localhost/simptis',
        remote: 'mongodb://lukasor:lukasor654@ds021915.mlab.com:21915/simptis',
        jackServer: 'mongodb://simptis:AhbezeICOjxRYpo1QeNm@jack.metal.agh.edu.pl:27017/simptis'
    },
    dirPaths: {
        public: '/public',
        dev: '/app',
        app_data: {
            materials: '/APP_DATA/materials',
            processes: '/APP_DATA/processes',
            main: '/APP_DATA',
            fullPath: ''
        }
    },
    solver: {
        commands: [{
            cmd: 'module load opencv/3.1.0-gcc520'
        }, {
            cmd: 'module load deal.ii/8.4.1-gcc52'
        }, {
            cmd: '../../../../solver/build/simptis.Debug',
            args: []
        }],
        scriptPath: '../../../../server/config/solverScript.sh',
        consoleLogFile: 'console.log',
        resultZipArchiveName: 'results.zip'
    },
    inputFile: {
        name: 'data.sim',
        templatePath: './server/config/input.tmpl.sim'
    },
    outputFile: {
        name: 'info.txt'
    },
    structureImageFile: {
        name: 'StructureImage'
    }
};