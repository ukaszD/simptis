#!/bin/bash
echo "Loading libraries..."
eval module load deal.ii/8.4.1-gcc52
eval module load opencv/3.1.0-gcc520
echo "Libraries loaded"
echo "Starting solver..."
eval ../../../../solver/build/simptis.Debug
echo "Solver stopped"