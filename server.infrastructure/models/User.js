var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    username: String,
    password: String,
    createAccountDate: {type: Date, "default": Date.now},
    email: String,
    role: String
});

userSchema.pre('save', function (next) {
    var error = null;
    // Required fields validation
    this.username ? error = this.password ? null : error = new Error("Password missing") : error = new Error("Username missing");

    next(error);
});

var User = mongoose.model('User', userSchema);
module.exports = User;
