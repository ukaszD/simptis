var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var chemicalElementSchema = new Schema({
    name: String,
    symbol: String
});

chemicalElementSchema.pre('save', function(next) {
    var error = null;
    this.symbol ? error = null : error = new Error("Symbol missing");
    next(error);
});

var ChemicalElement = mongoose.model('ChemicalElement', chemicalElementSchema);

module.exports = ChemicalElement;
