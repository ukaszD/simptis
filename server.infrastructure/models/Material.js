const mongoose = require('mongoose');
const fs = require('fs');
const config = require('../config/config');
const Schema = mongoose.Schema;

//TODO PREAPARE_IMAGE_NAME replace to other place - use one function
let getFileExtension = function (filename) {
  return (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename) : null;
};

let materialSchema = new Schema({
  name: String,
  description: String,
  chemicalIngredients: [{
    element: {},
    contens: Number
  }],
  balanceChart: {lineGS: String, lineSE: String, lineGP: String},
  diffusionCoefficient: String,
  createDate: {type: Date, "default": Date.now},
  lastModification: {type: Date, "default": Date.now},
  structureImage: {name: String, path: String, metadata: {}},
  owner: {type: Schema.Types.ObjectId, ref: 'User'}
});

materialSchema.pre('save', function (next, metadata) {
  let error = null;
  let destinationPath = '';
  if (metadata.imageFile) {
    let newDir = '.' + config.dirPaths.app_data.materials;
    //Making dir for matrial image file
    let materialId = this._id;
    fs.mkdir(newDir, function (err) {
      if (!err || err.code === 'EEXIST') {
        newDir += '/' + metadata.ownerId;
        fs.mkdir(newDir, function (err) {
          if (!err || err.code === 'EEXIST') {
            newDir += '/' + materialId;
            fs.mkdir(newDir, function (err) {
              if (!err || err.code === 'EEXIST') {
                console.log(getFileExtension(metadata.imageFile.name));
                //TODO PREAPARE_IMAGE_NAME replace to other place - use one function
                destinationPath += newDir + '/' + config.structureImageFile.name + '_' + metadata.timeTick + '.' + getFileExtension(metadata.imageFile.name);

                console.log("File tmp path on server: " + metadata.imageFile.path);
                //Move structure image file to ./APP_DATA dir
                fs.rename(metadata.imageFile.path, destinationPath, function (err) {
                  if (err) {
                    console.log("[error] File tmp rename error: " + err);
                    error = new Error(err);
                  }
                });

              } else {
                //TODO add
              }
            });
          } else {
            //TODO add
          }
        });
      } else {
        //TODO add
      }
    });
    this.structureImage.name = config.structureImageFile.name + '_' + materialId + '.' + getFileExtension(metadata.imageFile.name);
    this.structureImage.path = newDir + '/' + metadata.ownerId + '/' + materialId + '/' + config.structureImageFile.name + '_' + metadata.timeTick + '.' + getFileExtension(metadata.imageFile.name);
  }
  // Required fields validation
  this.name ? error = null : error = new Error("Name missing");
  next(error);
});

const Material = mongoose.model('Material', materialSchema);
module.exports = Material;
