var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var appConfig = new Schema({
    name: String,
    admin: {},
    lastLoggedUser: {}
});

appConfig.pre('save', function(next) {
    var error = null;

    next(error);
});

var AppConfig = mongoose.model('AppConfig', appConfig);

module.exports = AppConfig;
