var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var processSchema = new Schema({
    name: String,
    description: String,
    type: String,
    material: { type: Schema.Types.ObjectId, ref: 'Material' },
    createDate: {type: Date, "default": Date.now},
    heatingCycle: Schema.Types.Mixed,
    coolingCycle: Schema.Types.Mixed,
    temperingCycle: Schema.Types.Mixed,
    periodic: Boolean,
    spaceWidth: Number,
    spaceHeight: Number,
    elementCountHeight: Number,
    elementCountWidth: Number,
    diffusionTimeStep: Number,
    levelSetTimeStep: Number,
    interfaceRefinementLevel: Number,
    textResult: Boolean,
    paraview: Boolean,
    pid: Number,
    owner: { type: Schema.Types.ObjectId, ref: 'User' }
});

processSchema.pre('save', function (next) {
    var error = null;
    this.pid = -1;
    this.name ? error = null : error = new Error("Name missing");
    next(error);
});

var Process = mongoose.model('Process', processSchema);

module.exports = Process;
