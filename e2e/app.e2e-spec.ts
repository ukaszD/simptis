import { SimptisPage } from './app.po';

describe('simptis App', () => {
  let page: SimptisPage;

  beforeEach(() => {
    page = new SimptisPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
